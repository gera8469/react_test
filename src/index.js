import React from 'react';
import ReactDOM from 'react-dom';
import Field from './components/Field';
import './styles/styles.css';

ReactDOM.render(
    <Field/>,
    document.getElementById('root')
);
